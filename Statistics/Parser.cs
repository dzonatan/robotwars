﻿using System;
using System.Collections.Generic;

namespace RobotWars
{
    public class Parser
    {

        private readonly Parser _successor;

        public Parser(Parser successor)
        {
            _successor = successor;
        }

        public Parser() { }

        public virtual Dictionary<string, RobotStatistic> Parse(string fileName)
        {
            if (GetSuccessor() != null)
            {
                GetSuccessor().Parse(fileName);
            }
            else
            {
                Console.WriteLine("Unable to find the correct parser for the file: " + fileName);
            }
            throw new NotImplementedException();
        }

        protected bool CanHandleFile(string fileName, string format)
        {
            return (fileName == null) || (fileName.EndsWith(format));

        }

        Parser GetSuccessor()
        {
            return _successor;
        }
    }
}
