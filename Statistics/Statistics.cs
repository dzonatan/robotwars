﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RobotWars
{
    public class Statistics
    {
        private Dictionary<string, RobotStatistic> _fightsDictionary = new Dictionary<string, RobotStatistic>();

        private static Statistics _instance;
        public static Statistics Instance
        {
            get { return _instance ?? (_instance = new Statistics()); }
        }

        private Statistics()
        {
        }

        public void AddFight(string robotName, bool? isWinner)
        {
            if (!_fightsDictionary.ContainsKey(robotName))
            {
                _fightsDictionary.Add(robotName, new RobotStatistic());
            }

            if (isWinner.HasValue)
            {
                if ((bool)isWinner)
                {
                    _fightsDictionary[robotName].Wins++;
                }
                else
                {
                    _fightsDictionary[robotName].Looses++;
                }
            }
            else
            {
                _fightsDictionary[robotName].Draws++;
            }
        }

        public void ShowStatistics()
        {
            Console.WriteLine("");
            Console.WriteLine("# Robot statistics #");
            if (_fightsDictionary.Count == 0)
            {
                Console.WriteLine("No statistics");
                return;
            }

            foreach (var robotStatistic in _fightsDictionary)
            {
                Console.WriteLine("Robot `{0}` had {1} fights: {2} wins / {3} looses / {4} draws", robotStatistic.Key, robotStatistic.Value.Total, robotStatistic.Value.Wins, robotStatistic.Value.Looses, robotStatistic.Value.Draws);
            }
        }

        public void ExportStatistics(StatisticsExporter exporter, string filePath)
        {
            exporter.Export(filePath, _fightsDictionary);
        }


        public void ImportStatistics(string filePath)
        {
            var csvParser = new CsvParser(); ;
            var jsonParser = new JsonParser(csvParser);
            jsonParser.Parse(filePath);
        }

        public int GetRobotWinsCount(string robotName)
        {
            if (!_fightsDictionary.ContainsKey(robotName))
            {
                return 0;
            }

            return _fightsDictionary[robotName].Wins;
        }

        public void ClearStatistics()
        {
            _fightsDictionary.Clear();
        }
    }

    public class RobotStatistic
    {
        public int Wins { get; set; }
        public int Looses { get; set; }
        public int Draws { get; set; }
        public int Total
        {
            get { return Wins + Looses + Draws; }
        }
    }
}
