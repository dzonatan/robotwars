﻿using System;
using System.Collections.Generic;

namespace RobotWars
{
    public class JsonParser : Parser
    {
        public JsonParser(Parser successor)
            : base(successor)
        {
        }

        public override Dictionary<string, RobotStatistic> Parse(string fileName)
        {
            if (CanHandleFile(fileName, ".json"))
            {
                throw new NotImplementedException();
            }
            else
            {
                base.Parse(fileName);
            }
            throw new NotImplementedException();
        }

    }
}
