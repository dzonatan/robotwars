﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RobotWars
{
    public abstract class StatisticsExporter
    {
        protected abstract string BuildString(Dictionary<string, RobotStatistic> data);

        public void Export(string path, Dictionary<string, RobotStatistic> data)
        {
            string buildedString = BuildString(data);

            try
            {
                File.WriteAllText(path, buildedString);
            }
            catch (IOException ioException)
            {
                Console.WriteLine("Error writing file: " + ioException);
            }
        }
    }
}
