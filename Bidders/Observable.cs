﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RobotWars.Bidders
{
    public abstract class Observable
    {
        private string _winner;

        private readonly List<IBidder> _bidders = new List<IBidder>();

        public void Attach(IBidder bidder)
        {
            _bidders.Add(bidder);
        }

        public void Detach(IBidder bidder)
        {
            _bidders.Remove(bidder);
        }

        private void Notify()
        {
            foreach (IBidder bidder in _bidders)
            {
                bidder.Update(this);
            }
        }

        public string Winner
        {
            get { return _winner; }
            set
            {
                if (_winner != value)
                {
                    _winner = value;
                    Notify();
                }
            }
        }
    }
}
