﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RobotWars.Bidders
{
    class Bidder : IBidder
    {
        private readonly string _name;
        private readonly string _robotName;

        public Bidder(string name, string robotName)
        {
            _name = name;
            _robotName = robotName;
        }

        public void Update(Observable obesrvable)
        {
            string status = "lost";

            if (obesrvable.Winner == _robotName)
            {
                status = "won";
            }

            Console.WriteLine("Notify: {0} has {1} the bet with robot {2}!", _name, status, _robotName);            
        }
    }
}
