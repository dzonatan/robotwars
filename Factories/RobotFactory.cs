﻿using System;
using RobotWars.Robots;

namespace RobotWars.Factories
{
    public class RobotFactory
    {
        public IRobot CreateRobot(RobotType robotType, string robotName)
        {
            switch (robotType)
            {
                case RobotType.KickBoxer:
                    return new KickBoxer { Name = robotName };

                case RobotType.SmartBoxer:
                    return new SmartRobot { Name = robotName };

                default:
                    throw new NotImplementedException(string.Format("Robot {0} is not implemented!", robotType));
            }
        }
    }
}
