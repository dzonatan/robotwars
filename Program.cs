﻿using System;
using RobotWars.Bidders;
using RobotWars.Builder;
using RobotWars.Commands;
using RobotWars.Commentators;
using RobotWars.Factories;
using RobotWars.Robots;

namespace RobotWars
{
    static class Program
    {
        static void Main()
        {
            Console.WriteLine("## RobotWars ##");

            var arenaDirector = new ArenaDirector();
            var arenaBuilder = new ArenaBuilder();
            Arena arena = null;

            string nextCommand = String.Empty;
            while (nextCommand != "q")
            {
                Console.WriteLine("\n\nWhat can I help you?");
                Console.WriteLine("d - Set default arena settings");
                Console.WriteLine("c - Set arena custom settings");
                Console.WriteLine("s - start new fight");
                Console.WriteLine("t - see statistics");
                Console.WriteLine("p - post to social");
                Console.WriteLine("a - admin panel");
                Console.WriteLine("q - exit");
                Console.Write("Please enter you choise: ");

                nextCommand = Console.ReadLine();


                switch (nextCommand)
                {
                    case "d":
                        arena = arenaBuilder.DefaultArena();
                        break;

                    case "a":
                        arenaDirector.SetCommand(new AdminCommand(arena));
                        arenaDirector.ExecuteCommand();
                        break;

                    case "c":
                        arena = arenaBuilder.CustomArena(arenaDirector);
                        break;

                    case "s":
                        if (arena != null)
                        {
                            arenaDirector.SetCommand(new StartFightCommand(arena));
                            arenaDirector.ExecuteCommand();
                        }
                        else
                        {
                            Console.WriteLine("Please set arena settings first");
                        }
                        break;

                    case "t":
                        Statistics.Instance.ShowStatistics();
                        break;

                    case "p":
                        arenaDirector.SetCommand(new PostSocialMediaCommand(arena));
                        arenaDirector.ExecuteCommand();
                        break;

                    case "q":
                        Console.WriteLine("\nBye!");
                        break;

                    default:
                        Console.WriteLine("\nNo such command.");
                        break;
                }
            }
        }

    }
}
