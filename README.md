﻿## Grupę sudaro

Vardas Pavardė | Grupė | El. paštas
------------- | ------------- | ------------- |
Algirdas Jonas Kancleris | IFF-1 | [a.kancleris@gmail.com](mailto:a.kancleris@gmail.com)
Rokas Brazdžionis | IFF-1 | [rokas.brazdzionis@gmail.com](mailto:rokas.brazdzionis@gmail.com)
Redas Jarušaitis | IFF-1 | [re.jarusaitis@gmail.com](mailto:re.jarusaitis@gmail.com)

## Turinys

* [Programos aprašymas](#markdown-header-programos-aprasymas)
* [Laboratorinis darbas nr. 1](#markdown-header-laboratorinis-darbas-nr-1)
    * [Singleton](#markdown-header-realizuoti-singleton-sablona)
        * [Diagrama](#markdown-header-diagrama)
        * [Pagrindimas](#markdown-header-pagrindimas)
* [Laboratorinis darbas nr. 2](#markdown-header-laboratorinis-darbas-nr-2)
    * [Factory](#markdown-header-realizuoti-factory-sablona)
        * [Diagrama](#markdown-header-diagrama_1)
        * [Pagrindimas](#markdown-header-pagrindimas_1)
    * [Observer](#markdown-header-realizuoti-observer-sablona)
        * [Diagrama](#markdown-header-diagrama_2)
        * [Pagrindimas](#markdown-header-pagrindimas_2)
    * [Proxy](#markdown-header-realizuoti-proxy-sablona)
        * [Diagrama](#markdown-header-diagrama_3)
        * [Pagrindimas](#markdown-header-pagrindimas_3)
* [Laboratorinis darbas nr. 3](#markdown-header-laboratorinis-darbas-nr-3)
    * [Strategy](#markdown-header-realizuoti-strategy-sablona)
        * [Diagrama](#markdown-header-diagrama_4)
        * [Pagrindimas](#markdown-header-pagrindimas_4)
    * [State](#markdown-header-realizuoti-state-sablona)
        * [Diagrama](#markdown-header-diagrama_5)
        * [Pagrindimas](#markdown-header-pagrindimas_5)
    * [Builder](#markdown-header-realizuoti-builder-sablona)
        * [Diagrama](#markdown-header-diagrama_6)
        * [Pagrindimas](#markdown-header-pagrindimas_6)
* [Laboratorinis darbas nr. 4](#markdown-header-laboratorinis-darbas-nr-4)
    * [Chain of responsibility](#markdown-header-realizuoti-chain-of-responsibility-sablona)
        * [Diagrama](#markdown-header-diagrama_7)
        * [Pagrindimas](#markdown-header-pagrindimas_7)
    * [Tempate method](#markdown-header-realizuoti-template-method-sablona)
        * [Diagrama](#markdown-header-diagrama_8)
        * [Pagrindimas](#markdown-header-pagrindimas_8)
    * [Command](#markdown-header-realizuoti-command-sablona)
        * [Diagrama](#markdown-header-diagrama_9)
        * [Pagrindimas](#markdown-header-pagrindimas_9)
    * [NullObject](#markdown-header-realizuoti-null-object-sablona)
        * [Pagrindimas](#markdown-header-pagrindimas_10)
    * [Adapter](#markdown-header-realizuoti-adapter-sablona)
        * [Diagrama](#markdown-header-diagrama_10)
        * [Pagrindimas](#markdown-header-pagrindimas_11)

## Programos aprašymas

Robotu karai, tai programa - žaidimas, kurioje vienu metu gali kautis 2 robotai. Tai žaidimas skirtas programuotojams. Robotai turi turėti savo klasę ir realizuoti IRobot sąsają, kuri turi metodą MakeNextMove. Klasės realizuodamos šį metodą aprašo savo roboto logiką. Taigi, kiekvienas iš žaidėjų gali apsirašyti savo roboto klasę ir logiką, kuri "susikaus" su kito žaidėjo parašyta roboto implementacija.

## Laboratorinis darbas nr. 1

#### Realizuoti **Singleton** šabloną.

###### Diagrama

![Singleton diagrama](https://bytebucket.org/dzonatan/robotwars/raw/a59bd7b64dc3a8cbee3406128db90915a3424433/Diagrams/SingletonPattern.PNG)

###### Pagrindimas

Singleton šablonas užtikrina, kad būtų sukurtas tik viena klasės instancija. Mūsų programoje tai yra klasė "Statistics", kuri užtikrina, kad kovų statistikos bus kaupiamos į/naudojamos iš tos pačios vietos.

## Laboratorinis darbas nr. 2

#### Realizuoti **Factory** šabloną.

###### Diagrama

![Factory diagrama](https://bytebucket.org/dzonatan/robotwars/raw/a59bd7b64dc3a8cbee3406128db90915a3424433/Diagrams/FactoryPattern.PNG)

###### Pagrindimas

Factory šablonas skirtas izoliuoti objektų kūrimą atskiroje kasėje nuo kliento. Factory šabloną sudaro klasė RobotFactory, kuri turi metodą CreateRobot ir grąžina IRobot sąsają realizuojantį objektą, šiuo atveju KickBoxer ir SmartRobot.

#### Realizuoti **Observer** šabloną.

###### Diagrama

![Observer diagrama](https://bytebucket.org/dzonatan/robotwars/raw/a59bd7b64dc3a8cbee3406128db90915a3424433/Diagrams/ObserverPattern.PNG)

###### Pagrindimas

Observer šablonas pritaikytas žaidimo lošimo funkcionalumui įgyvendinti. Turime abstrakčią klasę Bet, kuria paveldėję realizuojame lošimą, ir turime klasę Bidder skirtą lošėjams, ji paveldi
sąsają IBidder. Sukuriame lošėjus, kuriuos priskiriame lošimui ir po žaidimo pabaigos lošimas praneša lošėjams apie pergalingą robotą. Tokiu būdų, naudodami Observer šabloną, galime sukurti 
kelis lošimus, kuriuose dalyvauja tie patys lošėjai.

#### Realizuoti **Proxy** šabloną.

###### Diagrama

![Proxy diagrama](https://bytebucket.org/dzonatan/robotwars/raw/0274664bcc5941aa31e7a29a53588f89551860ca/Diagrams/ProxyPattern.PNG)

###### Pagrindimas

Proxy šablonas šitame projekte naudojamas nesuteikti tiesioginės prieigos prie administratoriaus komandų. Šablonas suteikia galimybe taikyti autorizaciją norimiems veiksmas. Turime abstrakčią klasę Actions, kurioje aprašome norimus metodus. AdminActions klasėje realizuojam metodus, kurie įvykdomi be jokios autorizacijos. Proxy klasė taip pat paveldi abstrakčią klasę Actions, kurioje aprašoma autorizacijos dalis, ir tik jeigu autorizacija teisinga, leidžia vykdyti komandą.
Šablono didelis privalumas, kad autorizaciją ir komandos vykdymą galime atskirti į dvi skirtingas klases, nesurišant logikos, todėl galime paprastai atjungi autorizacija ar keisti jos logiką.

## Laboratorinis darbas nr. 3

#### Realizuoti **Strategy** šabloną.

###### Diagrama

![Startegy diagrama](https://bytebucket.org/dzonatan/robotwars/raw/a59bd7b64dc3a8cbee3406128db90915a3424433/Diagrams/StrategyPattern.PNG)

###### Pagrindimas

Strategy šablonas skirtas atskirti tam tikras implementacijas (algoritmus) nuo kliento, kurias jas naudoja. Turime abstrakčią klasę Commentator, kurią paveldi klasės BriefCommentator ir DetailedCommentator. Šias implementacijas mes nurodome klasei Arena per SetCommentator metodą. Tokiu atveju galime pildyti Commentator implementacijas nekeisdami Arena klasės. 

#### Realizuoti **State** šabloną.

###### Diagrama

![State diagrama](https://bytebucket.org/dzonatan/robotwars/raw/a59bd7b64dc3a8cbee3406128db90915a3424433/Diagrams/StatePattern.PNG)

###### Pagrindimas

State šablonas leidžia keisti objekto elgseną, kai pasikeičia tam tikra būsena. Šiuo atveju mes turime eglsenos sąsają IRobotBehaviour ir jos implementacijas DefensiveSmartRobot ir AggresiveSmartRobot, šios eglsenos nustatomos SmartRobot klasei. DefensiveSmartRobot ir AggresiveSmartRobot skaičiuoja taškus ir pagal juos nustato SmartRobot elgseną.

#### Realizuoti **Builder** šabloną.

###### Diagrama

![Builder diagrama](https://bytebucket.org/dzonatan/robotwars/raw/a59bd7b64dc3a8cbee3406128db90915a3424433/Diagrams/BuilderPattern.PNG)

###### Pagrindimas

Builder šablonas leidžia sukurtį sudėtingą objektą naudojant paprastą objektą, tai yra visa objekto kūrimo logika perkeliama į paprastesnę klasę. Turime ArenaBuilder klasę, kuri gali sukurti "default" arba "custom" arenas.

## Laboratorinis darbas nr. 4

#### Realizuoti **Chain of responsibility** šabloną.

###### Diagrama

![Chain of responsibility diagrama](https://bytebucket.org/dzonatan/robotwars/raw/3396ed954097c9993f62ae4c0476f778f0226def/Diagrams/ChainOfResponsibility.PNG)

###### Pagrindimas

Chain of responsibility šablonas atskiria pranešėją nuo gavėjo duodamas kitiems objektams (gavėjams) priimti pranešimą. Gautas pranešimas siunčiamas objektų grandine tol, kol yra apdorojamas. Turime klasę Parser ir implementacijas JsonParser ir CsvParser. JsonParser turi konstruktorių į kurį paduodame kitą Parser implentaciją, šiuo atveju CsvParser, taip gauname parserių grandinę. Siunčiamas failas pirmiausiai bandomas apdoroti JsonParser, jeigu failo formatas netinkamas, jis siunčiamas CsvParser.

#### Realizuoti **Template method** šabloną.

###### Diagrama

![Template method diagrama](https://bytebucket.org/dzonatan/robotwars/raw/4eb3b4cbdba322f0865c58f47119427c00b5055c/Diagrams/TemplatePattern.PNG)

###### Pagrindimas

Template method šablonas nusako algoritmo seką, bet leidžia sub-klasėm perrašyti tam tikrus algoritmo žingsnius nekeičiant algoritmo struktūros. Turime abstrakčią klasę StatisticsExporter, kuri turi Export metodą, nuskantį eksportavimo logiką (algoritmo seką) ir ją paveldinčias klases StatisticsExporterCsv ir StatisticsExporterJson, kurios turi metodą BuildString (vienas iš eksportavimo logikos žingsnių).

#### Realizuoti **Command** šabloną.

###### Diagrama

![Template method diagrama](https://bytebucket.org/dzonatan/robotwars/raw/a59bd7b64dc3a8cbee3406128db90915a3424433/Diagrams/CommandPattern.PNG)

###### Pagrindimas

Command šablonas leidžia inkapsuliuoti komandas (request) atskirame objekte. Turime abstrakčią klasę Command ir ją paveldinčias klases PostSocialMediaCommand, SetBiddersCommand, SetCommentatorCommand ir StartFightCommand. Šios klasės į konstruktorių gauna Arena objektą (receiver), kuris naudojamas komandai įvykdyti. Taip pat turime klasę ArenaDirector (invoker), kuris tą komandą gauna ir įvykdo. 

#### Realizuoti **Null Object** šabloną.

###### Pagrindimas

NullObject šablonas realizuotas Command šablone tuo atveju, jeigu užklausta komanda neegzistuoja.

#### Realizuoti **Adapter** šabloną.

###### Diagrama

![Adapter method diagrama](https://bytebucket.org/dzonatan/robotwars/raw/705f38005c67373964aded17de3703b835906245/Diagrams/AdapterDiagram.PNG)

###### Pagrindimas

Adapter šablonas naudojamas integruoti išorines socialinių tinklų klases/bibliotekas. Turime Interface klasę ISocialProvider, kuri naudojama SocialService klasėje. 
Turi dar dvi klases (adapterius): FacebookProvider ir TwitterProvider, kurios yra adaptuojamos mūsų standartiniai SocialService klasei. Adapteriui paduodame orginalų tiekėjo modelį, kurį adaptuojame mūsų sukurtam socialinių tinklų servisui.