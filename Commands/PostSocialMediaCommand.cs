﻿using System;
using RobotWars.Social;

namespace RobotWars.Commands
{
    class PostSocialMediaCommand : Command
    {
        public PostSocialMediaCommand(Arena arena)
            : base(arena)
        {
        }

        public override void Execute()
        {
            while (true)
            {
                Console.WriteLine("\nChoose social network:");
                Console.WriteLine("f - Facebook");
                Console.WriteLine("t - Twitter");
                Console.WriteLine("q - Back to main menu");
                Console.Write("Enter your choise: ");

                var userCommand = Console.ReadLine();

                switch (userCommand)
                {
                    case "f":
                        this.DoSocialMedia(userCommand);
                        break;

                    case "t":
                        this.DoSocialMedia(userCommand);
                        break;

                    case "n":
                        Console.WriteLine("\nThank you for posting!");
                        break;

                    default:
                        Console.WriteLine("\nNo such social media.");
                        break;
                }

                if (userCommand == "q")
                {
                    break;
                }
            }
        }

        public void DoSocialMedia(String userCommand)
        {
            Console.Write("Enter your robot name: ");
            var robotName = Console.ReadLine();

            SocialService socialService;

            switch (userCommand)
            {
                case "f":
                    socialService = new SocialService(new FacebookProvider());
                    break;

                case "t":
                    socialService = new SocialService(new FacebookProvider());
                    break;

                default:
                    throw new NotImplementedException("There is no such provider!");
            }

            socialService.ShareRobotScore(robotName, Statistics.Instance.GetRobotWinsCount(robotName));
        }
    }
}
