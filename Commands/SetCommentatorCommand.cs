﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RobotWars.Bidders;
using RobotWars.Commentators;
using RobotWars.Factories;
using RobotWars.Robots;

namespace RobotWars.Commands
{
    class SetCommentatorCommand : Command
    {
        public SetCommentatorCommand(Arena arena)
            : base(arena)
        {
        }

        public override void Execute()
        {
            Commentator commentator = SelectCommentator();
            _arena.SetCommentator(commentator);
        }

        static Commentator SelectCommentator()
        {
            Console.WriteLine();
            Console.WriteLine("Possible commentators:");
            Console.WriteLine("1. Brief");
            Console.WriteLine("2. Detailed");
            Console.Write("Please select the format number: ");

            int commentatorNumber = Convert.ToInt32(Console.ReadLine());

            switch (commentatorNumber)
            {
                case 1:
                    return new BriefCommentator();
                case 2:
                    return new DetailedCommentator();
                default:
                    throw new NotImplementedException(string.Format("Commentator {0} is not implemented!", commentatorNumber));
            }
        }
    }
}
