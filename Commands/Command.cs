﻿namespace RobotWars.Commands
{

    public abstract class Command
    {
        protected Arena _arena;

        protected Command(Arena arena)
        {
            _arena = arena;
        }


        public abstract void Execute();
    }
}
