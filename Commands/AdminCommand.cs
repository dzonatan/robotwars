﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RobotWars.Admin;

namespace RobotWars.Commands
{
    class AdminCommand : Command
    {
        public AdminCommand(Arena arena)
            : base(arena)
        {
        }

        public override void Execute()
        {
            while (true)
            {
                string adminCommand = String.Empty;

                Console.WriteLine("\nChoose admin action:");
                Console.WriteLine("c - Clear stats");
                Console.WriteLine("q - Back to main menu");
                Console.Write("Enter your choise: ");

                adminCommand = Console.ReadLine();

                switch (adminCommand)
                {
                    case "c":
                        ClearStats();
                        break;

                    case "q":
                        Console.WriteLine("\nExiting admin panel.");
                        break;

                    default:
                        Console.WriteLine("\nNo such action.");
                        break;
                }

                if (adminCommand == "q")
                {
                    break;
                }
            }          
        }

        public void ClearStats()
        {
            Console.WriteLine("\nPlease authorize.");
            Console.Write("Enter password: ");

            string password = String.Empty;
            password = Console.ReadLine();

            AdminProxy adminActions = new AdminProxy();
            adminActions.SetAdmin(password);
            string response = adminActions.ClearStats();

            Console.WriteLine("\n" + response);
        }
    }
}
