﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RobotWars.Bidders;
using RobotWars.Commentators;
using RobotWars.Factories;
using RobotWars.Robots;

namespace RobotWars.Commands
{
    class EmptyCommand : Command
    {
        public EmptyCommand(Arena arena)
            : base(arena)
        {
        }

        public override void Execute()
        {
           // Do nothing
        }
    }
}
