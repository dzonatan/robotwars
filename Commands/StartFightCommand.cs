﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RobotWars.Bidders;
using RobotWars.Commentators;
using RobotWars.Factories;
using RobotWars.Robots;

namespace RobotWars.Commands
{
    class StartFightCommand : Command
    {
        public StartFightCommand(Arena arena) : base(arena)
        {
        }

        public override void Execute()
        {
            // Create robots
            var factory = new RobotFactory();
            IRobot userRobot = GetUserRobot(factory);
            IRobot systemRobot = GetRandomRobot(factory);

            // Fight!
            _arena.Fight(userRobot, systemRobot);
        }

        private static IRobot GetUserRobot(RobotFactory factory)
        {
            // Select robot type
            Console.WriteLine();
            Console.WriteLine("Existing robots:");
            Console.WriteLine("1. Kickboxer");
            Console.WriteLine("2. Smart kickboxer");
            Console.Write("Please select the robot number: ");

            RobotType robotType = (RobotType)Convert.ToInt32(Console.ReadLine()) - 1;

            // Enter robot name
            Console.WriteLine();
            Console.Write("Please enter your robot name: ");
            string robotName = Console.ReadLine();

            return factory.CreateRobot(robotType, robotName);
        }

        static IRobot GetRandomRobot(RobotFactory factory)
        {
            var random = new Random(Guid.NewGuid().GetHashCode());
            var robotTypesCount = Enum.GetNames(typeof(RobotType)).Length - 1;
            var randomRobotType = (RobotType)random.Next(0, robotTypesCount);

            return factory.CreateRobot(randomRobotType, "System robot");
        }
    }
}
