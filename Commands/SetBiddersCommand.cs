﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RobotWars.Bidders;
using RobotWars.Commentators;
using RobotWars.Factories;
using RobotWars.Robots;

namespace RobotWars.Commands
{
    class SetBiddersCommand : Command
    {
        public SetBiddersCommand(Arena arena)
            : base(arena)
        {
        }

        public override void Execute()
        {
            while (true)
            {
                Console.WriteLine("\nTo stop adding bidders enter empty name.");
                Console.Write("Please enter your name: ");
                var bidderName = Console.ReadLine();

                if (bidderName == String.Empty)
                {
                    break;
                }

                Console.Write("Please enter robot name you bid on: ");
                var robotName = Console.ReadLine();

                _arena.Attach(new Bidder(bidderName, robotName));
            }
        }
    }
}
