﻿using System;

namespace RobotWars.SocialNetworks
{
    class Facebook
    {
        private const string FacebookName = "Robot Wars";

        public void PostOnWall(string message)
        {
            Console.WriteLine("\nFacebook: " + FacebookName + " Message: " + message);
        }
    }
}
