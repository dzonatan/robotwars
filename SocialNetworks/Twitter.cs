﻿using System;

namespace RobotWars.SocialNetworks
{
    class Twitter
    {
        private const String HashTag = "robotWars";

        public void TweetMessage(string message)
        {
            Console.WriteLine("\nTwitter " + " Message: " + message + " #" + HashTag);
        }
    }
}
