﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RobotWars.Admin
{
    class AdminProxy : Actions
    {
        private AdminActions _clearState;

        private String _password = "admin123";

        private bool _admin = false;

        public void SetAdmin(string password)
        {
            if (_password.Equals(password))
            {
                _admin = true;
            }
            else
            {
                _admin = false;
            }
        }

        public bool IsAdmin()
        {
            return _admin;
        }

        public String ClearStats()
        {
            if (_clearState == null) 
            {
                _clearState = new AdminActions();
            }

            return IsAdmin() ? _clearState.ClearStats() : "Bad authorization.";
        }
    }
}
