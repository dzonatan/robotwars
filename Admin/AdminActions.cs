﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RobotWars.Admin
{
    class AdminActions : Actions
    {
        public String ClearStats()
        {
            Statistics.Instance.ClearStatistics();

            return "Statistic is cleared.";
        }
    }
}
