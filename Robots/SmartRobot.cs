﻿
using System;

namespace RobotWars.Robots
{
    public class SmartRobot : IRobot
    {
        private IRobotBehaviour _behaviour;
        public string Name { get; set; }

        public SmartRobot()
        {
            _behaviour = new DefensiveSmartRobot();
        }

        public void SetBehaviour(IRobotBehaviour behaviour)
        {
            _behaviour = behaviour;
        }

        public Move MakeNextMove(Move opponentLastMove, int myLastScore, int oppLastScore)
        {
            return _behaviour.MakeNextMove(this, opponentLastMove, myLastScore, oppLastScore);
        }
    }

    public interface IRobotBehaviour
    {
        Move MakeNextMove(SmartRobot robot, Move opponentsLastMove, int myLastScore, int opponentsLastScore);
    }

    class DefensiveSmartRobot : IRobotBehaviour
    {
        public Move MakeNextMove(SmartRobot robot, Move opponentLastMove, int myLastScore, int oppLastScore)
        {
            var move = new Move();

            move.AddAttack(GetRandomTarget());
            move.AddDefence(GetRandomTarget());
            move.AddDefence(GetRandomTarget());

            if (myLastScore < oppLastScore)
            {
                robot.SetBehaviour(new AggresiveSmartRobot());
            }
            
            return move;
        }

        private Target GetRandomTarget()
        {
            double random = new Random(Guid.NewGuid().GetHashCode()).NextDouble();

            if (random < 0.2)
                return Target.Nose;

            if (random < 0.4)
                return Target.Jaw;

            if (random < 0.6)
                return Target.Groin;

            if (random < 0.8)
                return Target.Legs;

            return Target.Belly;
        }
    }

    class AggresiveSmartRobot : IRobotBehaviour
    {
        public Move MakeNextMove(SmartRobot robot, Move opponentLastMove, int myLastScore, int oppLastScore)
        {
            var move = new Move();

            for (int i = 0; i < 3; i++)
            {
                if (GetRandomBool())
                {
                    move.AddAttack(GetRandomTarget());
                }
                else
                {
                    move.AddDefence(GetRandomTarget());
                }
            }

            if (myLastScore > oppLastScore)
            {
                robot.SetBehaviour(new DefensiveSmartRobot());
            }

            return move;
        }

        private Target GetRandomTarget()
        {
            double random = new Random(Guid.NewGuid().GetHashCode()).NextDouble();

            if (random < 0.2)
                return Target.Nose;

            if (random < 0.4)
                return Target.Jaw;

            if (random < 0.6)
                return Target.Groin;

            if (random < 0.8)
                return Target.Legs;

            return Target.Belly;
        }

        private bool GetRandomBool()
        {
            double random = new Random(Guid.NewGuid().GetHashCode()).NextDouble();

            return random < 0.5;
        }
    }
}
