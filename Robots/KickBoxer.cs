﻿using System;

namespace RobotWars.Robots
{
    class KickBoxer : IRobot
    {
        public string Name { get; set; }

        public Move MakeNextMove(Move opponentLastMove, int myLastScore, int oppLastScore) 
        {
            var move = new Move();

            for (int i = 0; i < 3; i++)
            {
                if (GetRandomBool())
                {
                    move.AddAttack(GetRandomTarget());
                }
                else
                {
                    move.AddDefence(GetRandomTarget());
                }
            }

            return move;
        }

        private Target GetRandomTarget()
        {
            double random = new Random(Guid.NewGuid().GetHashCode()).NextDouble();

            if (random < 0.2)
                return Target.Nose;

            if (random < 0.4)
                return Target.Jaw;

            if (random < 0.6)
                return Target.Groin;

            if (random < 0.8)
                return Target.Legs;

            return Target.Belly;
        }

        private bool GetRandomBool()
        {
            double random = new Random(Guid.NewGuid().GetHashCode()).NextDouble();

            return random < 0.5;
        }
    }
}
