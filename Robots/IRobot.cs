﻿
namespace RobotWars.Robots
{
    public interface IRobot
    {
        string Name { get; set; }
        Move MakeNextMove(Move opponentsLastMove, int myLastScore, int opponentsLastScore);
    }
}
