﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RobotWars.Commentators
{
    class DetailedCommentator : Commentator
    {

        public override void GameOver(int f1Lifepoints, int f2Lifepoints)
        {
            Console.WriteLine("FIGHT OVER");

            if (f1Lifepoints > f2Lifepoints)
            {
                Console.WriteLine("THE WINNER IS '" + Fighter1 + "'!");
            }
            else if (f2Lifepoints > f1Lifepoints)
            {
                Console.WriteLine("THE WINNER IS '" + Fighter2 + "'!");
            }
            else
            {
                Console.WriteLine("IT'S A DRAW!!!");
            }
        }

        public override void DescribeRound(Move move1, Move move2, int score1, int score2, int figter1Lp, int fighter2Lp)
        {
            DescribeMove(Fighter1, move1, score1, move2);
            DescribeMove(Fighter2, move2, score2, move1);

            Console.WriteLine(Fighter1 + "(" + figter1Lp + ") vs " + Fighter2 + "(" + fighter2Lp + ")");
            Console.WriteLine();
        }

        private void DescribeMove(string fighterName, Move move, int score, Move counterMove)
        {
            Console.WriteLine(fighterName
                                    + DescribeAttacks(move, counterMove, score)
                                    + DescribeDefences(move));
        }

        private string DescribeAttacks(Move move, Move counterMove, int score)
        {
            if (move.Attacks.Count <= 0)
                return " did NOT attack at all ";

            string rez = " attacked ";
            foreach (Target attack in move.Attacks)
            {
                rez += attack;
                rez += counterMove.Defences.Contains(attack) ? "(-), " : "(+), ";
            }
            rez += "scoring " + score;

            return rez;
        }

        private string DescribeDefences(Move move)
        {
            if (move.Defences.Count <= 0)
            {
                return " and was NOT defending at all.";
            }

            string rez = " while defending ";
            foreach (Target defence in move.Defences)
            {
                rez += defence + ", ";
            }

            return rez;
        }
    }
}
