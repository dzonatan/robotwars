﻿using System;
using System.IO;

namespace RobotWars.Commentators
{
    public abstract class Commentator
    {
        protected string Fighter1;
        protected string Fighter2;

        public void SetFighterNames(string fighter1Name, string fighter2Name)
        {
            Fighter1 = fighter1Name;
            Fighter2 = fighter2Name;
        }

        public abstract void DescribeRound(Move move1, Move move2, int score1, int score2, int figter1Lp, int fighter2Lp);

        public abstract void GameOver(int f1Lifepoints, int f2Lifepoints);
    }
}
