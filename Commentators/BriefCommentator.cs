﻿using System;
using System.IO;

namespace RobotWars.Commentators
{
    class BriefCommentator : Commentator
    {
        public override void GameOver(int f1Lifepoints, int f2Lifepoints)
        {
            Console.WriteLine("");
            if (f1Lifepoints > f2Lifepoints)
            {
                Console.WriteLine("THE WINNER IS '" + Fighter1 + "'!");
            }
            else if (f2Lifepoints > f1Lifepoints)
            {
                Console.WriteLine("THE WINNER IS '" + Fighter2 + "'!");
            }
            else
            {
                Console.WriteLine("IT'S A DRAW!!!");
            }
            Console.WriteLine("");
        }

        public override void DescribeRound(Move move1, Move move2, int score1, int score2, int figter1Lp, int fighter2Lp)
        {
            Console.WriteLine(Fighter1 + "(" + figter1Lp + ") vs " + Fighter2 + "(" + fighter2Lp + ")");
        }
    }
}
