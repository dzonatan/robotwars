﻿using System.Collections.Generic;

namespace RobotWars
{
    static class GameScoringRules
    {
        public const int Lifepoints = 150;

        public static int CalculateScore(IEnumerable<Target> attackAreas, IList<Target> blockAreas)
        {
            int rez = 0;

            if (attackAreas == null)
            {
                return rez; 
            }

            foreach (Target attack in attackAreas)
            {
                if (blockAreas.Contains(attack) == false)
                {
                    rez += (int)attack;
                }
            }

            return rez;
        }

	    public static bool IsMoveLegal(Move move) 
        {
		    return ( move.Attacks.Count + move.Defences.Count <= 3 );
	    }
    }
}