﻿using System;
using RobotWars.Commands;

namespace RobotWars.Builder
{
    public class ArenaBuilder
    {
        public Arena DefaultArena()
        {
            var arena = new Arena();

            Console.Write("Default arena settings are set\n");

            return arena;
        }

        public Arena CustomArena(ArenaDirector arenaDirector)
        {
            var arena = new Arena();
            string nextCommand = String.Empty;
            while (nextCommand != "q")
            {
                Console.WriteLine("\n\nWhat can I help you?");
                Console.WriteLine("c - set commentator");
                Console.WriteLine("b - set bidders");
                Console.WriteLine("q - quit builder");
                Console.Write("Please enter you choice: ");

                nextCommand = Console.ReadLine();
                switch (nextCommand)
                {
                    case "c":
                        arenaDirector.SetCommand(new SetCommentatorCommand(arena));
                        break;

                    case "b":
                        arenaDirector.SetCommand(new SetBiddersCommand(arena));
                        break;

                    default:
                        arenaDirector.SetCommand(new EmptyCommand(arena));
                        break;
                }
                arenaDirector.ExecuteCommand();
            }
            Console.Write("Custom arena settings are set\n");

            return arena;
        }
    }
}
