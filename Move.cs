﻿using System.Collections.Generic;
using System.Text;

namespace RobotWars
{
    public class Move
    {
        private readonly List<Target> _attacks = new List<Target>();
        private readonly List<Target> _defences = new List<Target>();

        public IList<Target> Attacks
        {
            get
            {
                return _attacks;
            }
        }

        public IList<Target> Defences
        {
            get
            {
                return _defences;
            }
        }

        public Move AddAttack(Target area)
        {
            _attacks.Add(area);
            return this;
        }

        public Move AddDefence(Target area)
        {
            _defences.Add(area);
            return this;
        }
        
        public override string ToString()
        {
            var rez = new StringBuilder("Move ");

            foreach (Target attack in _attacks)
            {
                rez.Append(" ATTACK " + attack);
            }

            foreach (Target defence in _defences)
            {
                rez.Append(" BLOCK " + defence);
            }

            return rez.ToString();
        }
    }
}