﻿using System;
using System.Threading;
using RobotWars.Bidders;
using RobotWars.Commentators;
using RobotWars.Robots;

namespace RobotWars
{
    public class Arena : Observable
    {
        private Commentator _commentator;

        public void Fight(IRobot fighter1, IRobot fighter2) 
        {
            if (_commentator != null)
            {
                _commentator.SetFighterNames(fighter1.Name, fighter2.Name);
            }
            
		    Move f1Move = null;
		    Move f2Move = null;
		
		    int score1 = 0;
		    int score2 = 0;
		
		    int f1Lifepoints = GameScoringRules.Lifepoints;
		    int f2Lifepoints = GameScoringRules.Lifepoints;
		
		    while (f1Lifepoints > 0 && f2Lifepoints > 0)
            {
			    Move move1 = fighter1.MakeNextMove(f2Move, score1, score2);
			    if (!GameScoringRules.IsMoveLegal(move1))
                    throw new ArgumentException(fighter1.GetType().Name + " made an illegal move: " + move1);

                Thread.Sleep(100);

                Move move2 = fighter2.MakeNextMove(f1Move, score2, score1);
			    if (!GameScoringRules.IsMoveLegal(move2))
                    throw new ArgumentException(fighter2.GetType().Name + " made an illegal move: " + move2);
			
			    score1 = GameScoringRules.CalculateScore(move1.Attacks, move2.Defences);
			    score2 = GameScoringRules.CalculateScore(move2.Attacks, move1.Defences);

                f1Lifepoints -=score2;
                f2Lifepoints -= score1;

                if (_commentator != null)
                {
                    _commentator.DescribeRound(move1, move2, score1, score2, f1Lifepoints, f2Lifepoints);
                }
			
			    f1Move = move1;
			    f2Move = move2;

                Thread.Sleep(100);
		    }

            if (_commentator != null)
            {
                _commentator.GameOver(f1Lifepoints, f2Lifepoints);
            }

            // Set the winner
            if (f1Lifepoints > f2Lifepoints)
            {
                Winner = fighter1.Name;
                Statistics.Instance.AddFight(fighter1.Name, true);
                Statistics.Instance.AddFight(fighter2.Name, false);
            }
            else if (f1Lifepoints < f2Lifepoints)
            {
                Winner = fighter2.Name;
                Statistics.Instance.AddFight(fighter1.Name, false);
                Statistics.Instance.AddFight(fighter2.Name, true);
            }
            else
            {
                Winner = "it's a draw.";
                Statistics.Instance.AddFight(fighter1.Name, null);
                Statistics.Instance.AddFight(fighter2.Name, null);
            }
        }

        public void SetCommentator(Commentator commentator)
        {
            _commentator = commentator;
        }
    }
}
