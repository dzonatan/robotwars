﻿using RobotWars.SocialNetworks;

namespace RobotWars.Social
{
    class FacebookProvider : ISocialProvider
    {
        private readonly Facebook _facebook = new Facebook();

        public void PostMessage(string message)
        {
            _facebook.PostOnWall(message);
        }
    }
}
