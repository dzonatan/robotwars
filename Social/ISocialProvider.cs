﻿namespace RobotWars.Social
{
    interface ISocialProvider
    {
        void PostMessage(string message);
    }
}
