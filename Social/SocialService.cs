﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RobotWars.Social;
using RobotWars.SocialNetworks;

namespace RobotWars.Social
{
    class SocialService
    {
        private readonly ISocialProvider _socialProvider;

        public SocialService(ISocialProvider socialProvider)
        {
            _socialProvider = socialProvider;
        }

        public void ShareRobotScore(string robotName, int winCount)
        {
            _socialProvider.PostMessage(robotName + " won " + winCount + " time(s)!");
        }
    }
}
