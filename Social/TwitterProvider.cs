﻿using RobotWars.SocialNetworks;

namespace RobotWars.Social
{
    class TwitterProvider : ISocialProvider
    {
        private readonly Twitter _twitter = new Twitter();

        public void PostMessage(string message)
        {
            _twitter.TweetMessage(message);
        }
    }
}
